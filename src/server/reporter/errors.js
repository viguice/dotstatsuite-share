import { omit } from 'ramda';
import debug from '../debug';

export const ERROR = 'ADD_ERROR';

const reducer = (state, action) => {
  try {
    switch (action.type) {
      case ERROR:
        return [{ ...omit(['type'], action), time: new Date() }, ...state];
      default:
        return state;
    }
  } catch (e) {
    debug.error(e);
  }
};

export default reducer;
