import Report from '../index';
import { START } from '../process';
import { ERROR } from '../errors';

describe('Reporter', () => {
  it('should start report', () => {
    const report = Report();
    report({ type: START });
    const r = report();
    expect(r.process.startTime).toBeDefined();
  });
});
