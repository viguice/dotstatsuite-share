import { createRootReducer } from './reducer';
import process from './process';
import errors from './errors';

const rootReducer = {
  errors,
  process,
};

const initialState = {
  process: {},
  errors: [],
};

const report = createRootReducer(initialState, rootReducer);
const Report = () => action => (!action ? report.getState() : report.dispatch(action));
export default Report;
