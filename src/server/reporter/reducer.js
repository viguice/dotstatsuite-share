import { toPairs, reduce } from 'ramda';

export const createRootReducer = (initalState, reducer) => {
  let state = initalState;
  const root = reducer;

  return {
    getState: () => state,
    dispatch: action => {
      state = reduce((acc, [key, fn]) => ({ ...acc, [key]: fn(acc[key], action) }), state, toPairs(root));
    },
  };
};
