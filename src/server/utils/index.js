import * as R from 'ramda';

// export const deepMerge = (a, b) => (is(Object, a) && is(Object, b) ? mergeWith(deepMerge, a, b) : b);
export const isTestEnv = R.propEq('env', 'test');

const evalBool = R.cond([[R.equals('true'), R.T], [R.equals('false'), R.F], [R.T, R.identity]]);

export const getSMTPEnv = env => {
  const keys = R.keys(env);
  return R.reduce((acc, key) => {
    if (
      R.pipe(
        R.match(/^SMTP_/),
        R.isEmpty,
      )(key)
    )
      return acc;
    const path = R.pipe(
      R.replace(/^SMTP_/, ''),
      R.split('_'),
    )(key);
    return R.assocPath(path, evalBool(R.prop(key)(env)), acc);
  }, {})(keys);
};
