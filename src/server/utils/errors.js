function HTTPError(code, message) {
  Error.captureStackTrace(this, HTTPError);
  this.message = message || `HTTP Error code ${code}`;
  this.name = 'HTTPError';
  this.code = code;
}

HTTPError.prototype = Object.create(Error.prototype);
HTTPError.prototype.constructor = HTTPError;

module.exports = { HTTPError };
