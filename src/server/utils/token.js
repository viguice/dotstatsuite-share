import njwt from 'njwt';

export const getToken = ({ id, email }, secretKey, expirationDate) => {
  const claims = { sub: id, email };
  const jwt = njwt.create(claims, secretKey);
  jwt.setExpiration(expirationDate);
  return jwt.compact();
};
