import * as Yup from 'yup'
import * as R from 'ramda'
import { emitEvent, validate } from './utils'
import { ChartNotFoundError } from '../../models/utils'
import { HTTPError } from '../../utils/errors'

const addSchema = Yup.object().shape({
  email: Yup.string()
    .email()
    .required(),
  confirmUrl: Yup.string()
    .url()
    .required(),
  data: Yup.object({})
    .noUnknown(false)
    .required(),
})

const SERVICE_NAME = 'charts'

export const chart = {
  async post({ email, data, confirmUrl, logo, content, subject, css }) {
    const {
      models,
      config: { confirmUrl: url },
    } = this.globals
    return await models.charts.add({ email, data, confirmUrl: confirmUrl || url, logo, content, subject, css })
  },

  async get({ id, token }) {
    const { models } = this.globals
    try {
      if (id) {
        const chart = await models.charts.get(id)
        return R.omit(['email', 'logo'], chart)
      } else if (token) return await models.charts.listFromToken(token)
      else throw new HTTPError(400, '"id" or "token" are required search params to get charts!')
    } catch (e) {
      if (e instanceof ChartNotFoundError) throw new HTTPError(404, e.message)
      throw e
    }
  },

  async delete({ token, id }) {
    const { models } = this.globals
    try {
      if (!token) throw new HTTPError(400, '"token" is a required search params to delete charts!')
      if (id) return await models.charts.delete({ token, id })
      else return await models.charts.deleteAll({ token })
    } catch (e) {
      if (e instanceof ChartNotFoundError) throw new HTTPError(404, e.message)
      throw e
    }
  },
}

const init = evtx => {
  evtx.use(SERVICE_NAME, chart)
  evtx
    .service(SERVICE_NAME)
    .before({
      post: [validate(addSchema)],
    })
    .after({
      post: [emitEvent('chart:added')],
    })
}

export default init
