import { reduce } from 'ramda'
import evtX from 'evtx'
import initCharts from './charts'
import initConfirm from './confirm'
import initMail from './mail'

const allServices = [initMail, initCharts, initConfirm]

const initServices = evtx => reduce((acc, service) => acc.configure(service), evtx, allServices)
const init = ctx => {
  const api = evtX(ctx()).configure(initServices)
  return ctx({ services: { ...ctx().services, api } })
}

export default init
