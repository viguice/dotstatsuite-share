import { emitEvent } from './utils'

const SERVICE_NAME = 'mail'

export const list = {
  async post({ id, email, logo, content, subject, css }) {
    return { email: id || email, logo, content, subject, css }
  },

  async get({ id, email }) {
    return { email: id || email }
  },
}

const init = evtx => {
  evtx.use(SERVICE_NAME, list)
  evtx.service(SERVICE_NAME).after({
    post: [emitEvent('chart:sendToken')],
    get: [emitEvent('chart:sendToken')],
  })
}

export default init
