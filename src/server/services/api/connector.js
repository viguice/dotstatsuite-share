import R from 'ramda'
import { UnknownMethodError, UnknownServiceError } from 'evtx'
import { ValidationError } from 'yup'
import { HTTPError } from '../../utils/errors'

const getVerb = req => req.method.toLowerCase()
const getInput = (req, pos) => R.mergeAll([req.query, req.body, pos])
const parseUrl = url => {
  const re = new RegExp(/^\/+(\w+)\/*(.*)/)
  const [_, service, id] = re.exec(url) // eslint-disable-line no-unused-vars
  return R.map(x => x || undefined, [service, id])
}

export const getMessage = req => {
  let path = req.path
  if (path.match(/^\/charts\/confirm/)) path = path.replace('/charts', '')

  const [service, id] = parseUrl(path)
  const input = getInput(req, id ? { id } : {})
  const verb = getVerb(req)
  return { service, method: verb, input }
}

const server = evtx => (req, res, next) => {
  if (!evtx) return next(new Error('EvtX is not configured!'))
  evtx
    .run(getMessage(req), { req, res })
    .then(data => res.json(data))
    .catch(e => {
      if (e instanceof UnknownMethodError) return next(new HTTPError(404))
      if (e instanceof UnknownServiceError) return next(new HTTPError(404))
      if (e instanceof ValidationError) return next(new HTTPError(400, e.message))
      if (e.code) return next(e)
      next(e)
    })
}

export default server
