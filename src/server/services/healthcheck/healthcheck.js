import debug from '../../debug.js';

const NAME = 'healthcheck';

async function doHealthcheck() {
  const {
    startTime,
    config: { env, gitHash },
    mongo,
  } = this.globals();

  const res = {
    startTime,
    env,
    gitHash,
    mongo: 'OK',
  };

  try {
    await mongo.ping();
  } catch (err) {
    debug.error(err);
    res.mongo = 'KO';
  }

  return res;
}

const healthcheck = {
  get: doHealthcheck,
};

export default evtx => evtx.use(NAME, healthcheck);
