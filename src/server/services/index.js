import { initResources } from 'jeto';
import initApi from './api';
import initAdmin from './admin';
import initHealthcheck from './healthcheck';

export default ctx => initResources([() => ctx, initApi, initAdmin, initHealthcheck]);
