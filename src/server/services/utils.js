import { reduce } from 'ramda';
export const initServices = services => evtx => reduce((acc, service) => acc.configure(service), evtx, services);
