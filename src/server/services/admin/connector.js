import R from 'ramda';
import { UnknownMethodError, UnknownServiceError } from 'evtx';

const getVerb = req => req.method.toLowerCase();
const getInput = req => R.mergeAll([req.query, req.body]);
const parseUrl = url => {
  const re = new RegExp(/^\/+(\w+)\/*(\w*)/);
  const [_, service, method] = re.exec(url); // eslint-disable-line no-unused-vars
  return R.map(x => x || undefined, [service, method]);
};

export const getMessage = req => {
  const [service, method] = parseUrl(req.path);
  const input = getInput(req);
  const verb = getVerb(req);
  return { service, method: verb, input };
};

const server = evtx => (req, res, next) => {
  if (!evtx) return next(new Error('EvtX is not configured!'));
  evtx
    .run(getMessage(req), { req })
    .then(data => res.json(data))
    .catch(e => {
      if (e instanceof UnknownMethodError) return next();
      if (e instanceof UnknownServiceError) return next();
      next(e);
    });
};

export default server;
