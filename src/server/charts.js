import { propEq } from 'ramda'
import urlJoin from 'url-join'

export const status = {
  PENDING: 'PENDING',
  CONFIRMED: 'CONFIRMED',
}

export const isPending = propEq('status', status.PENDING)

export const makeConfirmUrl = (siteUrl = '') => urlJoin(siteUrl, '/api/charts/confirm')
