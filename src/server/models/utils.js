import { path, multiply, pipe } from 'ramda'
import { parse, toSeconds } from 'iso8601-duration'
import { v4 as uuid } from 'uuid'
export const parseISO8601 = isoDuration => {
  if (!isoDuration) return 0
  return pipe(parse, toSeconds, multiply(1000))(isoDuration)
}

export const getChartTitle = path(['headerProps', 'title', 'label'])

export function ChartNotFoundError(id, message) {
  Error.captureStackTrace(this, ChartNotFoundError)
  this.message = message || `"${id}" is not a valid chart's id`
  this.name = 'ChartNotFoundError'
  this.id = id
}
ChartNotFoundError.prototype = Object.create(Error.prototype)
ChartNotFoundError.prototype.constructor = ChartNotFoundError

export function ChartProcessingError(message) {
  Error.captureStackTrace(this, ChartProcessingError)
  this.message = message || `Cannot process chart request`
  this.name = 'ChartProcessingError'
}
ChartProcessingError.prototype = Object.create(Error.prototype)
ChartProcessingError.prototype.constructor = ChartProcessingError

export function IsoError(message) {
  Error.captureStackTrace(this, IsoError)
  this.message = message || 'Iso Error'
  this.name = 'IsoError'
}
IsoError.prototype = Object.create(Error.prototype)
IsoError.prototype.constructor = IsoError

export const ID = id => (id ? String(id) : uuid())
