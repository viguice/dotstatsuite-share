import { reduce } from 'ramda'
import Chart from './charts'

const providers = [Chart]

export default async ctx => {
  const { config, mongo } = ctx()

  const models = await reduce(
    async (acc, provider) => {
      const res = await acc
      const [name, model, { indexes } = {}] = await provider({ config, mongo })
      indexes && (await model.ensureIndexes(indexes))
      return { ...res, [name]: model }
    },
    Promise.resolve({}),
    providers,
  )

  return models
}
