import debug from '../../debug'

const html = ({ content, css = '' }) => {
  return `
    <!doctype html>
      <html>
        <head>
         <style>${css}</style>
       </head>
     <body>
     <div>${content}</div>
     </body>
     </html>
  `
}

export const sendEmail = async ({ token, send, mailFrom, hfrom, mailOptions, content }) => {
  const options = {
    ...mailOptions,
    token,
    from: mailFrom,
    html: html({ content }),
    headers: {
      hfrom,
    },
  }
  return send(options)
    .then(() => debug.info(`email sent to '${options.to}'`))
    .catch(err => debug.error(err))
}
