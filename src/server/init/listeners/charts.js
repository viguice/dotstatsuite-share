import { addSeconds } from 'date-fns'
import { prop, path } from 'ramda'
import { getToken } from '../../utils/token'
import { sendEmail } from './utils'
import debug from '../../debug.js'
import skeleton from './mails/skeleton'
import getHeader from './mails/header'
import getBody from './mails/body'
import getFooter from './mails/footer'

const SESSION_DURATION = 1000

const sendToken = (send, ctx) => async ectx => {
  const {
    output: chart,
    globals: {
      config: { hfrom, mailFrom, secretKey, redisChartsTTL },
    },
  } = ectx
  const {
    config: { siteUrl },
  } = ctx()
  const expires = addSeconds(new Date(), redisChartsTTL || SESSION_DURATION)
  const token = getToken(chart, secretKey, expires)
  const email = prop('email', chart)
  const id = prop('id', chart)
  const banner = path(['css', 'banner'], chart)
  const bodyColor = path(['css', 'body'], chart)
  const buttonColor = path(['css', 'button'], chart)
  const logo = prop('logo', chart)
  const base = `${siteUrl}/?token=${token}&email=${email}`
  const content = skeleton({
    header: getHeader({ logo, banner }),
    body: getBody({
      link: id ? `${base}&id=${id}` : base,
      headerMsg: path(['content', 'headerMsg'], chart),
      subHeaderMsg: path(['content', 'subHeaderMsg'], chart),
      copyMsg: path(['content', 'copyMsg'], chart),
      confirmationText: path(['content', 'confirmationText'], chart),
      buttonMsg: path(['content', 'buttonMsg'], chart),
      bodyColor,
      buttonColor,
    }),
    footer: getFooter({ banner }),
  })

  return sendEmail({
    send,
    token,
    content,
    hfrom,
    mailFrom,
    mailOptions: {
      subject: prop('subject', chart),
      to: email,
    },
  })
}

export default ctx => {
  const {
    email: { send },
    services: { api },
  } = ctx()
  api.service('charts').on('chart:added', sendToken(send, ctx))
  api.service('mail').on('chart:sendToken', sendToken(send, ctx))

  debug.info('charts listeners up')
  return ctx
}
