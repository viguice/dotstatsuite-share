export default ({ banner }) => `
<table width="600" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td style="background: ${banner}; padding: 10px; text-align: center;">
        <a  style="color: white;" rel="noopener noreferrer" href="#">
        </a>
      </td>
    </tr>
  </tbody>
</table>`;
