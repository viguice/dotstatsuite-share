import nodemailer from 'nodemailer';
import debug from '../debug';

const mailDefaultOptions = {
  from: 'Share<share@siscc.com>',
};

const fakeSendMail = () => Promise.resolve();

export const sendMail = options => {
  if (!options) return fakeSendMail;
  const transporter = nodemailer.createTransport(options);
  return mailOptions => {
    const promise = new Promise((resolve, reject) => {
      transporter.sendMail({ ...mailDefaultOptions, ...mailOptions }, (err, res) => {
        if (err) return reject(err);
        debug.info(`Mail started`);
        resolve(res);
      });
    });
    return promise;
  };
};

export default ctx => {
  const {
    config: { smtp },
  } = ctx();
  return ctx({ email: { send: sendMail(smtp) } });
};
