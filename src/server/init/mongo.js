import { MongoClient } from 'mongodb'
import debug from '../debug.js'

export default async ctx => {
  const {
    config: {
      mongo: { dbName, url },
    },
  } = ctx()
  const mongoClient = new MongoClient(url, { readPreference: 'nearest' })
  await mongoClient.connect()
  debug.info('connected to mongodb server')
  return ctx({
    mongo: {
      client: mongoClient,
      database: mongoClient.db(dbName),
      ping: () => mongoClient.db(dbName).command({ ping: 1 }),
      dropDatabase: () => mongoClient.db(dbName).dropDatabase(),
      clearTestDbs: async () => {
        const res = await mongoClient
          .db(dbName)
          .admin()
          .listDatabases({ nameOnly: true, filter: { name: /^test-/ } })
        for (const db of res.databases) {
          await mongoClient.db(db.name).dropDatabase()
        }
      },
      close: () => mongoClient.close(),
    },
  })
}
