import debug from '../debug';
import Report from '../reporter';
import { START } from '../reporter/process';

export default ctx => {
  const report = Report();
  report({ type: START });
  debug.info('reporter on site');
  return ctx({ report });
};
