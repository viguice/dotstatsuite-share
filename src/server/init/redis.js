const debug = require('../debug');
const Redis = require('ioredis');

export default async ctx => {
  const {
    config: { redisServer: conf },
  } = ctx();

  const redisServer = await new Redis(conf);
  debug.info(conf.sentinels ? 'redis sentinel connected' : 'redis connected');
  return ctx({ redisServer });
};
