import debug from '../../../debug';

const error = (err, req, res, next) => {
  if (!err) return next();
  debug.error(err);
  return res.sendStatus(err.code || 500);
};

module.exports = error;
