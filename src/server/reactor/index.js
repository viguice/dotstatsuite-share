import debug from '../debug';

const sendSSE = res => {
  let seq = 1;
  return ({ comment, event, data }) => {
    if (comment) res.write(`: ${comment}\n\n`);
    if (event || data) {
      res.write(`id: ${seq++}\n`);
      if (event) res.write(`event: ${event}\n`);
      if (data) res.write(`data: ${JSON.stringify(data)}\n\n`);
    }
  };
};

const Reactor = ctx => {
  const clients = {};
  const {
    services: { api },
  } = ctx;

  return (req, res) => {
    const {
      user,
      query: { id },
    } = req;

    res.on('close', () => {
      delete clients[id];
      debug.info(`Remove '${user.fullname()}' as listener`);
    });

    const ping = () => {
      const client = clients[id];
      if (!client) return;
      client.send({ comment: 'ping' });
      setTimeout(() => ping(), 1000);
    };

    clients[id] = {
      id,
      user,
      res,
      date: new Date(),
      send: sendSSE(res),
    };
    debug.info(`Add '${user.fullname()}' as listener`);
    res.writeHead(200, {
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache',
      Connection: 'keep-alive',
    });
    ping();
  };
};

export default Reactor;
