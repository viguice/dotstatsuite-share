import axios from 'axios'
import { doAfterAll, doBeforeAll } from './utils'
import { status } from '../charts'
import { ID } from '../models/utils'

let CTX

describe('API status service', function() {
  beforeAll(async () => {
    CTX = await doBeforeAll()
  })

  afterAll(() => doAfterAll(CTX))

  it('should answer to healthcheck', () => {
    const { httpServer } = CTX
    return axios({
      method: 'get',
      url: `${httpServer.url}/healthcheck`,
    }).then(({ data }) => {
      expect(data.mongo).toEqual('OK')
    })
  })

  describe('Chart Lifecycle', function() {
    let chartId
    let chartId2
    const email = 'toto@titi.fr'
    const input = {
      email,
      confirmUrl: 'http://confirm.fr?a=1',
      data: { type: 'TYPE', config: { dataflowId: 'TUTU' }, headerProps: { title: { label: 'MyTitle1', c: 2 }, b: 3 } },
      logo: 'LOGO',
      content: 'CONTENT',
      subject: 'SUBJECT',
      css: 'CSS',
      from: 'FROM ME',
    }

    const input2 = {
      email,
      confirmUrl: 'http://confirm.fr?a=2',
      data: { config: { dataflowId: 'TOTO' }, headerProps: { title: { label: 'MyTitle2', c: 3 }, b: 6 } },
    }

    it('should add chart', async () => {
      const { httpServer } = CTX
      const { data } = await axios({ method: 'post', url: `${httpServer.url}/api/charts`, data: input })
      expect(data.email).toEqual(input.email)
      expect(data.confirmUrl).toEqual(input.confirmUrl)
      expect(data.status).toEqual(status.PENDING)
      expect(data.createdAt).toBeDefined()
      expect(data.logo).toEqual(input.logo)
      expect(data.content).toEqual(input.content)
      expect(data.subject).toEqual(input.subject)
      expect(data.css).toEqual(input.css)
      expect(data.id).toBeDefined()
      expect(data.data).toEqual({ dataflowId: 'TUTU', title: 'MyTitle1', type: 'TYPE' })
      expect(CTX.sendEmail).toHaveBeenCalledTimes(1)
      const mail = CTX.sendEmail.mock.calls[0][0]
      expect(mail.subject).toEqual(input.subject)
      expect(mail.to).toEqual(input.email)
      expect(mail.from).toEqual(input.from)
      chartId = data.id
    })

    it('should get chart with PENDING status', async () => {
      const { httpServer } = CTX
      const { data } = await axios({ method: 'get', url: `${httpServer.url}/api/charts/${chartId}` })
      expect(data.email).toBeUndefined()
      expect(data.confirmUrl).toEqual(input.confirmUrl)
      expect(data.status).toEqual(status.PENDING)
      expect(data.id).toEqual(chartId)
      expect(data.createdAt).toBeDefined()
      expect(data.lastAccessedAt).toBeDefined()
      expect(data.lastAccessedAt > data.createdAt).toBeTruthy()
      expect(data.expire > data.lastAccessedAt).toBeTruthy()
      expect(data.data).toEqual({ dataflowId: 'TUTU', title: 'MyTitle1', type: 'TYPE' })
    })

    it('should not confirm because of bad token', done => {
      const { httpServer } = CTX
      axios({ method: 'get', url: `${httpServer.url}/api/charts/confirm/myBadToken` }).catch(({ response }) => {
        expect(response.status).toEqual(401)
        done()
      })
    })

    it('should not return list charts (nothing confirmed yet)', () => {
      const { httpServer } = CTX
      return axios({ method: 'get', url: `${httpServer.url}/api/charts/?token=${CTX.testToken}` }).then(({ data }) => {
        expect(data.length).toEqual(0)
      })
    })

    it('should confirm and get chart', async () => {
      const { httpServer } = CTX
      await axios({ method: 'get', url: `${httpServer.url}/api/charts/confirm?token=${CTX.testToken}` })
      const { data } = await axios({ method: 'get', url: `${httpServer.url}/api/charts/${chartId}` })
      expect(data.email).toBeUndefined()
      expect(data.confirmUrl).toEqual(input.confirmUrl)
      expect(data.status).toEqual(status.CONFIRMED)
      expect(data.id).toEqual(chartId)
      expect(data.data).toEqual(input.data)
      expect(data.createdAt).toBeDefined()
      expect(data.lastAccessedAt).toBeDefined()
      expect(data.accessCount).toEqual(1)
    })

    it('should send email', () => {
      const { httpServer } = CTX
      return axios({ method: 'post', url: `${httpServer.url}/api/mail/${email}` }).then(() => {
        expect(CTX.sendEmail).toHaveBeenCalledTimes(2)
      })
    })

    it('should return list charts', () => {
      const { httpServer } = CTX
      return axios({ method: 'get', url: `${httpServer.url}/api/charts/?token=${CTX.testToken}` }).then(({ data }) => {
        expect(data).toHaveLength(1)
      })
    })

    it('should unauthorize asked list', done => {
      const { httpServer } = CTX

      axios({ method: 'get', url: `${httpServer.url}/api/charts/?token=badToken` }).catch(({ response }) => {
        expect(response.status).toEqual(401)
        done()
      })
    })

    it('should post and confirm new chart ', () => {
      const { httpServer } = CTX
      CTX.sendEmail.mockClear()

      return axios({ method: 'post', url: `${httpServer.url}/api/charts`, data: input2 }).then(({ data }) => {
        expect(CTX.sendEmail).toHaveBeenCalledTimes(1)
        chartId2 = data.id
        return axios({ method: 'get', url: `${httpServer.url}/api/charts/confirm/${CTX.testToken}` })
      })
    })

    it('should unauthorize delete', done => {
      const { httpServer } = CTX

      axios({ method: 'delete', url: `${httpServer.url}/api/charts/${chartId}?token=badToken` }).catch(
        ({ response }) => {
          expect(response.status).toEqual(401)
          done()
        },
      )
    })

    it('shoudl delete chart', done => {
      const { httpServer } = CTX

      axios({
        method: 'delete',
        url: `${httpServer.url}/api/charts/${chartId}?token=${CTX.testToken}`,
      }).then(() =>
        axios({ method: 'get', url: `${httpServer.url}/api/charts/${chartId}` }).catch(({ response }) => {
          expect(response.status).toEqual(404)
          done()
        }),
      )
    })

    it('should still get other charts', () => {
      const { httpServer } = CTX
      return axios({ method: 'get', url: `${httpServer.url}/api/charts/${chartId2}` }).then(({ data }) => {
        expect(data.id).toBeDefined()
      })
    })

    it('delete all', () => {
      const { httpServer } = CTX

      return axios({ method: 'delete', url: `${httpServer.url}/api/charts/?token=${CTX.testToken}` }).then(() => {
        return axios({ method: 'get', url: `${httpServer.url}/api/charts/?token=${CTX.testToken}` }).then(
          ({ data }) => {
            expect(data.length).toEqual(0)
          },
        )
      })
    })

    it('should not add chart', done => {
      const { httpServer } = CTX
      const input = {
        confirmUrl: 'http://confirm.fr?a=1',
        data: { a: 1 },
      }
      axios({
        method: 'post',
        url: `${httpServer.url}/api/charts`,
        data: input,
      }).catch(() => done())
    })
  })

  describe('Admin', () => {
    const input = {
      data: { attr: 1 },
      confirmUrl: 'http://confirm.fr?a=1',
    }
    let chartId

    it('should add confirmed charts', async () => {
      const { httpServer } = CTX

      await axios({ method: 'post', url: `${httpServer.url}/api/charts`, data: { ...input, email: 'email1@toto.fr' } })
      await axios({ method: 'get', url: `${httpServer.url}/api/charts/confirm/${CTX.testToken}` })

      const { data } = await axios({
        method: 'post',
        url: `${httpServer.url}/api/charts`,
        data: { ...input, email: 'email2@toto.fr' },
      })
      chartId = data.id
      await axios({ method: 'get', url: `${httpServer.url}/api/charts/confirm/${CTX.testToken}` })
    })

    it('should not clear charts', async () => {
      const { httpServer } = CTX
      await axios({ method: 'delete', url: `${httpServer.url}/admin/charts?api-key=${CTX.config.apiKey}` })
    })

    it('should list existing charts', async () => {
      const { httpServer } = CTX
      await axios({ method: 'post', url: `${httpServer.url}/api/mail/email2@toto.fr` })
      const { data } = await axios({ method: 'get', url: `${httpServer.url}/api/charts/?token=${CTX.testToken}` })
      expect(data.length).toEqual(1)
    })

    it('should clear 1 chart', async () => {
      const { httpServer, models } = CTX
      await models.charts.collection.updateOne({ _id: ID(chartId) }, { $set: { expire: new Date(1999, 1, 1) } })
      await axios({ method: 'delete', url: `${httpServer.url}/admin/charts?api-key=${CTX.config.apiKey}` })
    })

    it('should list existing charts', async () => {
      const { httpServer } = CTX
      const { data } = await axios({
        method: 'get',
        url: `${httpServer.url}/admin/charts/?api-key=${CTX.config.apiKey}`,
      })
      expect(data.length).toEqual(1)
      expect(data[0].email).toEqual('email1@toto.fr')
    })
  })
})
