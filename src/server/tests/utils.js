import { initResources } from 'jeto'
import { v4 as uuid } from 'uuid'
import initHttp from '../init/http'
import initServices from '../services'
import initRedis from '../init/redis'
import initListeners from '../init/listeners'
import initMongo from '../init/mongo'
import initModels from '../init/models'

export const mockDate = () => {
  const mockedDate = new Date(2050, 11, 10)
  global.Date = class {
    constructor() {
      return mockedDate
    }
  }
  global.Date.now = () => mockedDate
}

// jest.mock('../init/listeners/utils', () => ({
//   sendEmail: ({ send, testToken }) => {
//     return send(testToken)
//   },
// }))

let CTX

const sendEmail = jest.fn(options => {
  // console.log('===========================sendEmail', options)
  CTX.testToken = options.token
  return Promise.resolve()
})

const initFakeMail = ctx => ctx({ email: { send: sendEmail } })

const initConfig = ctx =>
  ctx({
    config: {
      isProduction: 'production',
      outOfDate: 'P1Y',
      apiKey: 'secret',
      secretKey: 'secret',
      redisChartsTTL: 3600 * 4,
      chartsTTL: 3600 * 4,
      maxRatePerIP: 5,
      maxChartSize: '5000kb',
      redisServer: { host: 'localhost', port: 6379 },
      server: { host: '0.0.0.0' },
      hfrom: 'HFROM',
      mailFrom: 'FROM ME',
      mongo: {
        url: process.env.CI ? 'mongodb://mongo:27017' : 'mongodb://localhost:27017',
        dbName: `test-${uuid()}`,
      },
    },
  })

export const doBeforeAll = async () => {
  const resources = [
    initConfig,
    initRedis,
    initMongo,
    initModels,
    initFakeMail,
    initServices,
    initHttp,
    initListeners,
    ctx => ctx({ config: { ...ctx().config, siteUrl: ctx().httpServer.url } }),
  ]

  try {
    const ctx = await initResources(resources)
    CTX = { ...ctx(), sendEmail }
    return CTX
  } catch (err) {
    console.error(err) // eslint-disable-line
    if (CTX) {
      const { redisServer, httpServer, mongo } = CTX
      if (mongo) await mongo.dropDatabase().then(() => mongo.close())
      if (httpServer) await httpServer.close()
      if (redisServer) redisServer.disconnect()
    }
  }
}

export const doAfterAll = async CTX => {
  const { mongo, httpServer } = CTX
  return await mongo
    .dropDatabase()
    .then(() => mongo.clearTestDbs())
    .then(() => mongo.close())
    .then(() => httpServer.close())
}
