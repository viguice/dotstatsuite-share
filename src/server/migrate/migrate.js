import { dissoc } from 'ramda'
import debug from '../debug'
import { parseISO8601 } from '../models/utils'

const chartKey = id => `share:chart:${id}`

export default async ctx => {
  const { config, redisServer, models } = ctx()

  const xform = input => {
    const res = {
      ...dissoc('id', input),
      _id: input.id,
      accessCount: 0,
    }

    if (input.createdAt) res.createdAt = new Date(Number(input.createdAt))
    else input.createdAt = new Date()

    if (input.lastAccessedAt) res.lastAccessedAt = new Date(Number(input.lastAccessedAt))
    else res.lastAccessedAt = new Date()

    if (res.status !== 'PENDING') res.expire = new Date(+res.lastAccessedAt + parseISO8601(config.outOfDate))

    return res
  }

  const allkeys = await redisServer.keys(chartKey('*'))
  debug.info(`find ${allkeys.length} charts to migrate`)
  const migrated = []
  for (const key of allkeys) {
    try {
      const rchart = await redisServer.hgetall(key)
      const mchart = xform(rchart)
      await models.charts.collection.insertOne(mchart)
      debug.info({ rchart: dissoc('data', rchart), mchart: dissoc('data', mchart) }, 'chart migrated')
      migrated.push(mchart)
    } catch (err) {
      debug.error(err, `Cannot migrate redis chart#${key}`)
    }
  }
  debug.info(`${migrated.length}/${allkeys.length} chart(s) migrated`)
  if (migrated.length !== allkeys.length)
    debug.warn(`${allkeys.length - migrated.length} chart(s) NOT migrated due to errors`)
  debug.info('migration ended')
}
