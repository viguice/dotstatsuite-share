import '@babel/polyfill'
import { initResources } from 'jeto'
import debug from '../debug'
import initConfig from '../init/config'
import initRedis from '../init/redis'
import initMongo from '../init/mongo'
import initModels from '../init/models'
import migrate from './migrate'

const ressources = [
  initConfig,
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
  initRedis,
  initMongo,
  initModels,
  ctx => {
    debug.info("let's run migration")
    return ctx
  },
  ctx => migrate(ctx),
  () => process.exit(),
]

initResources(ressources).catch(err => {
  debug.error(err, 'migration failed')
  process.exit()
})
