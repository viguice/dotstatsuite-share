import request from 'request';

request(
  {
    method: 'POST',
    url: 'http://localhost:3007/api/charts',
    json: true,
    body: {
      email: 'eric.basley@darejs.fr',
      // confirmUrl: 'http://localhost:3007/test',
      data: {
        values: [1,2,3,4,5,6],
      }
    }
  },
  (err, res, body) => {
    if (err) return console.log(err);
    console.log(body);
  },
);
