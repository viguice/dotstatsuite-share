import '@babel/polyfill'
import { initResources } from 'jeto'
import debug from '../src/server/debug'
import initConfig from '../src/server/init/config'
import initRedis from '../src/server/init/redis'
import chartsManager from '../src/server/chartsManager'

const load = async ctx => {
  const cm = chartsManager(ctx())
  const input = {
    email: 'toto@titi.fr',
    confirmUrl: 'http://confirm.fr?a=1',
    data: { type: 'TYPE', config: { dataflowId: 'TUTU' }, headerProps: { title: { label: 'MyTitle1', c: 2 }, b: 3 } },
    logo: 'LOGO',
    content: 'CONTENT',
    subject: 'SUBJECT',
    css: 'CSS',
    from: 'FROM ME',
  }
  const chart = await cm.add(input)
  await cm.confirm(chart.id)
}

const ressources = [
  initConfig,
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
  initRedis,
  ctx => ctx({ config: { ...ctx(), redisChartsTTL: 10000 } }),
  ctx => load(ctx),
  () => process.exit(),
]

initResources(ressources).catch(err => {
  debug.error(err, 'load failed')
  process.exit()
})
