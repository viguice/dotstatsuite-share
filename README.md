# Share Server

Service called by [data-explorer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer) in order to upload data of a customized table or chart of a dataflow selected data. Once uploaded and confirmed, the data is made accessible, allowing [data-viewer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-viewer) to retrieve it and display the corresponding table or chart.

## Install

Install [nodejs](https://nodejs.org/en/)

Clone repository.

```
$ cd share
$ yarn
```

## Resources

A running `mongodb` server (version >= 6.3) must be reachable to run `share`.  

In dev mode to start one:

```
$ docker run -d --name=mongo --restart=always -p 27017:27017 mongo:7.0.1 
```

## Config

Config variables are located in `.env` in development mode else in the shell context.

```
{
  mail: {
    // SMTP_yourKey - see SMTP section to read more informations
  },
  mailFrom: process.env.MAIL_FROM,
  server: { host: process.env.HOST || '0.0.0.0', port: process.env.PORT || 3007 },
  siteUrl: `http://${server.host}:${server.port}`,
  maxChartSize: '5000kb',
  chartsTTL: 3600 * 4,
  outOfDate: process.env.OUT_OF_DATE || 'P1Y', // ISO 8601 duration
  hfrom: process.env.HFROM,  // see below
  apiKey: process.env.API_KEY || 'secret',
  secretKey: process.env.SECRET_KEY || 'secret',
  mongo: {
     url: process.env.MONGODB_URL || 'mongodb://localhost:27017',
     dbName: process.env.MONGODB_DATABASE || 'share',
  },
}
```

## SMTP

You can use your own email provider by using SMTP connection.
There are some environment variables available to your configuration:
  - MAIL_FROM: is the email use to let the user know who the email is from (example to '"Share" <share@siscc.org>')

To transport email we are using **[nodemailer](https://www.npmjs.com/package/nodemailer)**.
`smtp` environement variables have to be prefixed with **SMTP** to scope them and avoid collision with other variables

Below is an example of how you can setup SMTP with the share service (minimal config).  
You can find more configuration/explanation on [nodemailer smtp documentation](https://nodemailer.com/smtp/)  
```shell
SMTP_port=587
SMTP_host=domain
SMTP_auth_user=username
SMTP_auth_pass=userpassword
```
_Nodemailer is waiting for an object which can be created from env var by using `_` to create nested levels._
For developer, the configuration above will create the following object:
```js
{
  port: '587',
  host: 'domain',
  auth: {
    user: 'username',
    pass: 'userpassword',
  }
}
```

## Custom headers
  Currently the share service support one custom header: 
    - HFROM

you can use that property in your `.env`

## Start Server

Launch it in development mode:
```
$ yarn start:srv
[2023-09-28 14:47:14.870 +0200] INFO (share): running config
    appId: "share"
    isProduction: false
    outOfDate: "P1Y"
    gitHash: "#develop"
    env: "development"
    apiKey: ...
    secretKey: ... 
    chartsTTL: 14400
    maxChartSize: "5000kb"
    siteUrl: "http://0.0.0.0:3007"
    server: {
      "host": "0.0.0.0",
      "port": 3007
    }
    smtp: {}
    mongo: {
      "url": "mongodb://localhost:27017",
      "dbName": "share"
    }
[2023-09-28 14:47:14.873 +0200] INFO (share): reporter on site
[2023-09-28 14:47:14.895 +0200] INFO (share): connected to mongodb server
[2023-09-28 14:47:14.907 +0200] INFO (share): healthcheck service up.
[2023-09-28 14:47:14.914 +0200] INFO (share): server started on http://0.0.0.0:3007
[2023-09-28 14:47:14.915 +0200] INFO (share): charts listeners up
[2023-09-28 14:47:14.916 +0200] INFO (share): 🤖 server started

```

in production mode:

```
$ yarn dist:srv
$ yarn dist:run
...
```



### Health checks


```
$ curl http://localhost:3007/healthcheck
{
gitHash: "#develop",
mongo: "OK",
startTime: "2019-03-26T08:10:07.391Z"
}
```


### Test

A running `redis` server defined in `test` environment must be reachable to run tests.


To execute tests, run:
```
$ yarn test
```

To check coverage:
```
$ yarn coverage
```


## Publication Workflow

### Data model

* `id`: uniq chart's id as UUID or String(serial number) for existing charts created before Redis to MongoDB migration
* `email`: user's email
* `status: [PENDING, CONFIRMED]
* `data`: chart's data, partially hidden when chart is pending
* `confirmUrl`: url to redirect user about its chart usage (show case)
* `createdAt`
* `lastAccessedAt`: updated on each `get`
* `accessCount`: number of accesses (get) in CONFIRMED status
* `expire`: expire date computed as `lastAccessedAt + outOfDate`
* `logo`
* `content`
* `subject`
* `css`

NB: if chart is PENDING, chart.data will be overwrittent by `{ title, dataflowId: data.config.dataflowId, type: data.type }`


### Publication

* share's user request to publish a chart { email, data, confirmUrl }
* chart is temporally stored during `chartTTL` in a PENDING status
* email is sent to user to confirm publication
* before confirmation chart can be retreived from `share` but without data
* after chart's ttl, if not confirmed, chart is deleted

### Confirmation

* user receive a confirmation email about its publication
* to confirm, user click on confirmation link
* chart status is switched to CONFIRMED
* chart is now fully available
* user is redirected to 'confirmURL'

### Access

* everybody with 'chartUrl' can get it


## API

### HTTP API

#### `POST /api/charts `: create a chart

**request body**:

```
{
  email: [required] user's email used for confirmation
  data: [required] chart's data (limited to chartMaxSize)
  confirmUrl: [required] share server will redirect to this url after confirmation. `chartId` query params will be added. If value is undefined `params.confirUrl` will be used
  logo
  content
  subject
  css
}
```

**response**: [200, chart]

NB: chart.status === `PENDING`

#### `GET /api/charts/:id`: get a chart by id


**response**: [200, chart]

#### `GET /api/charts?token`: list all charts associated to email embedded in token

**response**: [200, [chart]]



#### `DELETE /api/charts/:id?token=token`: delete chart by id

**response**: [200, { id: deletedid }]


#### `DELETE /api/charts?token=token`: delete all charts associated to email embedded in token

**response**: [200, { deletedCount }]


#### `GET /api/charts/confirm?token`: confirm chart with id embedded in token

**response**: [200, { url: chart.confirmUrl }]

NB: returns 400 is chart is already CONFIRMED

#### `POST /api/mail/:email`: send an email with a link associated to a token that embedded user's email

**response**: [200, { email }]


RM: returns 400 is chart is already CONFIRMED


### HTTP Admin

#### `DELETE /admin/charts?api-key=<secret> `: delete all expired charts [ where expire > now ]

**response**: [200, { deletedCount }]
